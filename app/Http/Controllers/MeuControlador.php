<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MeuControlador extends Controller
{
    public function app(){
        return view('app');
    }
    public function user(){
        return view('user');
    }
    public function profile(){
        return view('profile');
    }
    public function produtos(){
        return view('produtos');
    }
    public function departamentos(){
        return view('departamentos');
    }
    public function multiplicar($n1, $n2){
        return $n1 * $n2;
    }
}
