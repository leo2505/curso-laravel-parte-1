@extends('layouts.principal')
@section('conteudo')

    <h3>Clientes</h3>
    <a href="{{ route('clientes.create') }}">Novo Cliente</a>
    @if(count($clientes)>0)
    <ul>
        @foreach ($clientes as $c)
            <li>
            {{$c['id']}} | {{$c['nome']}} | <a href="{{ route('clientes.edit', $c['id']) }}">Editar</a> <a href="{{ route('clientes.show', $c['id'])}}">Info</a>
            <form action="{{ route('clientes.destroy', $c['id']) }}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" value="Remover">
            </form>
            </li>
        @endforeach
    </ul>
    @else
        <h4>Não existem clientes cadastrados.</h4>
    @endif
    @empty($clientes)
    <p>0 Registos</p>
    @endempty
    <!-- @for ($i = 1; $i < count($clientes)+1; $i++)
        {{$i}}
    @endfor -->
    @foreach ($clientes as $item)
        @if ($loop->first)
            (primeiro) | 
        @endif
        <!-- ({{ $loop->index }}) - {{$loop->iteration}} / {{$loop->count}}| -->
        {{$loop->iteration}}
        @if ($loop->last)
            (último)
        @endif
        
    @endforeach

@endsection