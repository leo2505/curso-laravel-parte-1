@extends('layouts.principal')
@section('conteudo')

<div class="options">
    <ul>
        <!--<li><a class="warning {{request()->routeIs('opcoes') ? 'selected' : ''}}" href="{{ route('opcoes', 1)}}">Warning</a></li>-->
        <li><a class="warning" href="{{ route('opcoes', 1)}}">Warning</a></li>
        <li><a class="info" href="{{ route('opcoes', 2)}}">Info</a></li>
        <li><a class="success" href="{{ route('opcoes', 3)}}">Success</a></li>
        <li><a class="error" href="{{ route('opcoes', 4)}}">Error</a></li>
    </ul>
</div>

@if(isset($opcao))
    @switch($opcao)
        @case(1)
            @component('components.alerta', ['titulo'=> 'Alerta', 'tipo'=> 'warning'])
            <p>Verificar alerta</p>
            @endcomponent
            @break
        @case(2)
            @component('components.alerta', ['titulo'=> 'Informação', 'tipo'=> 'info'])
            <p>Verificar informação</p>
            @endcomponent
            @break
        @case(3)
            @component('components.alerta', ['titulo'=> 'Sucesso', 'tipo'=> 'success'])
            <p>Verificar sucesso</p>
            @endcomponent
            @break
        @case(4)
            @component('components.alerta', ['titulo'=> 'Erro', 'tipo'=> 'error'])
            <p>Verificar erro</p>
            @endcomponent
            @break
        @default
            
    @endswitch
@endif

@endsection