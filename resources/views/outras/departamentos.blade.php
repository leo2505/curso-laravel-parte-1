@extends('layouts.principal')
@section('conteudo')
<h3>Departamentos</h3>
<ul>
    <li>Computadores</li>
    <li>Eletrônicos</li>
    <li>Acessórios</li>
    <li>Roupas</li>
</ul>

@alerta(['titulo'=> 'Erro', 'tipo'=> 'error'])
<p>Verificar problema</p>
@endalerta

@component('components.alerta', ['titulo'=> 'Erro', 'tipo'=> 'info'])
<p>Verificar problema</p>
@endcomponent
@endsection