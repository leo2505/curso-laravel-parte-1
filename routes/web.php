<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('app', 'MeuControlador@app');
Route::get('multiplicar/{n1}/{n2}', 'MeuControlador@multiplicar');
Route::prefix('/app')->group(function(){
    Route::get('/', 'MeuControlador@app')->name('app');
    Route::get('/user', 'MeuControlador@user')->name('app-user');
    Route::get('/profile', 'MeuControlador@profile')->name('app-profile');
});
Route::get('produtos', function(){
    return view('outras.produtos');
})->name('produtos');
Route::get('departamentos', function(){
    return view('outras.departamentos');
})->name('departamentos');
Route::get('apps', function(){
    return redirect()->route('app');
});
Route::redirect('/', '/app', 301)->name('RedirectApp');

Route::resource('clientes', 'ClienteControlador');

Route::get('opcoes/{opcao?}',function($opcao=null){
    return view('outras.opcoes', compact(['opcao']));
})->name('opcoes');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('bootstrap', function(){
    return view('outras.exemplo');
});